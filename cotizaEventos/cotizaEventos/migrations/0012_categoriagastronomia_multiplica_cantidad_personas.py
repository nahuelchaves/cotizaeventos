# Generated by Django 2.0.4 on 2018-08-15 16:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cotizaEventos', '0011_gastronomia_multiplica_cantidad_personas'),
    ]

    operations = [
        migrations.AddField(
            model_name='categoriagastronomia',
            name='multiplica_cantidad_personas',
            field=models.BooleanField(default=True),
        ),
    ]
