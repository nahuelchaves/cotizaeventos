from django.urls import re_path

from . import views

urlpatterns = [
    re_path(r'imprime/(?P<id>[0-9]+)/$', views.imprime_cotizacion),
    re_path(r'imprime5/(?P<id>[0-9]+)/$', views.imprime_cotizacion_5)
]
