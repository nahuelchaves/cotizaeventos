from django.apps import AppConfig

class CotizaEventosConfig(AppConfig):
    name = 'cotizaEventos'
    verbose_name = 'Cotizador'