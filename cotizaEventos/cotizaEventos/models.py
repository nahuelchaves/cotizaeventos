from django.db import models
from mptt.models import MPTTModel
from decimal import Decimal
# from config.models import Cotizacion as CotizacionConfig
import locale

# import the logging library
import logging

# Get an instance of a logger
logger = logging.getLogger(__name__)

locale.setlocale(locale.LC_ALL, "es_AR.UTF-8")

f = '{:n}'.format

class TipoDeEvento(models.Model):
    nombre = models.CharField(max_length=200)
    descripcion = models.CharField(max_length=200, blank=True, null=True, default=None)

    def __str__(self):
        return "%s (%s)" % (self.nombre, self.descripcion)


class Formato(models.Model):
    nombre = models.CharField(max_length=200)

    def __str__(self):
        return self.nombre


class Salon(models.Model):
    nombre = models.CharField(max_length=200)
    costo = models.DecimalField(default=0.00, max_digits=7, decimal_places=2)
    diagrama = models.FileField(upload_to='uploads/', blank=True, null=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Salones"


class Escenario(models.Model):
    nombre = models.CharField(max_length=200)
    descripcion = models.CharField(max_length=255, default="")
    costo = models.DecimalField(default=0.00, max_digits=7, decimal_places=2)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Escenarios"


class Cliente(models.Model):
    nombre = models.CharField(max_length=30)
    apellido = models.CharField(max_length=30)
    email = models.EmailField()
    telefono = models.CharField(max_length=30)

    def __str__(self):
        return self.nombre + " " + self.apellido


class CategoriaGastronomia(MPTTModel):
    nombre = models.CharField(max_length=100, unique=False)
    es_principal = models.BooleanField(default=False)
    parent = models.ForeignKey('self', related_name="categorias", null=True, blank=True, on_delete=models.CASCADE)
    orden = models.IntegerField(default=0, db_index=True)
    es_isla = models.BooleanField(default=False)
    costo = models.DecimalField(default=0.00, max_digits=7, decimal_places=2, help_text="Precio general o Adultos")
    costo_2 = models.DecimalField(default=0.00, max_digits=7, decimal_places=2, help_text="Precio Menores")
    imagen = models.CharField(max_length=100, blank=True, null=True, help_text="Nombre de la imagen para los PDF's (ejemplo: cocktail.jpg)")
    mostrar = models.BooleanField(default=True)
    adultos = models.BooleanField(default=False, verbose_name="Calcula total de Adultos", help_text="Se utiliza para calcular el total de Adultos")
    menores = models.BooleanField(default=False, verbose_name="Calcula total de Menores", help_text="Se utiliza para calcular el total de Menores")

    class MPTTMeta:
        order_insertion_by = ['orden']

    def __str__(self):
        return self.nombre

    @property
    def gastronomias_tienen_costo(self):
        costos = [x.costo for x in self.gastronomias.all()]
        return sum(costos) > 0



class Gastronomia(MPTTModel):
    nombre = models.CharField(max_length=100)
    descripcion = models.TextField(blank=True)
    con_cantidad = models.BooleanField(default=False)
    categoria = models.ForeignKey(CategoriaGastronomia, related_name="gastronomias", default=None, null=True, blank=True, on_delete=models.SET_NULL)
    parent = models.ForeignKey('self', related_name="gastronomias", null=True, blank=True, on_delete=models.CASCADE)
    incluida = models.BooleanField(default=False, blank=True)
    costo = models.DecimalField(default=0.00, max_digits=7, decimal_places=2)
    adultos = models.BooleanField(default=False, verbose_name="Calcula total de Adultos",
                                  help_text="Se utiliza para calcular el total de Adultos")
    menores = models.BooleanField(default=False, verbose_name="Calcula total de Menores",
                                  help_text="Se utiliza para calcular el total de Menores")
    suma_calculo_total = models.BooleanField(default=False, verbose_name="Se suma al total de la cotizacion")

    def __str__(self):
        categorias = ""
        #for node in self.categoria.get_family():
        #    categorias += node.nombre + " > "
        if self.categoria:
            if self.categoria.is_root_node():
                categorias = " - " + self.categoria.nombre
            else:
                for cat in list(self.categoria.get_family().values_list('nombre')):
                    categorias = categorias + " - " + cat[0]
            #return "%s - %s" % (categorias[2:], self.nombre)
        return self.nombre


class CotizacionLineas(models.Model):
    gastronomia = models.ForeignKey(Gastronomia, default=0, null=True, blank=True, on_delete=models.DO_NOTHING)
    valor = models.NullBooleanField(default=False, null=True, blank=True)
    cantidad = models.IntegerField(default=0, null=True, blank=True)
    categoria = models.ForeignKey(CategoriaGastronomia, on_delete=models.DO_NOTHING)


    def __str__(self):
        if self.gastronomia != None:
            if self.cantidad != None and self.cantidad > 0:
                return "%s -> %s ----  %s item/s" % (self.categoria.nombre, self.gastronomia.nombre, self.cantidad)
            return "%s -> %s" % (self.categoria.nombre, self.gastronomia.nombre)
        else:
            return "%s" % (self.categoria.nombre)



class Cotizacion(models.Model):
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    fecha = models.DateTimeField()
    observaciones = models.TextField(default="", blank=True)
    cantidad_adultos = models.IntegerField(default=0)
    cantidad_menores = models.IntegerField(default=0)
    cantidad_menores_de_4 = models.IntegerField(default=0)
    tipo_de_evento = models.ForeignKey(TipoDeEvento, default=None, on_delete=models.DO_NOTHING)
    formato = models.ForeignKey(Formato, default=None, on_delete=models.DO_NOTHING)
    salon = models.ForeignKey(Salon, default=None, on_delete=models.DO_NOTHING)
    escenario = models.ForeignKey(Escenario, default=None, on_delete=models.DO_NOTHING)

    lineas = models.ManyToManyField(CotizacionLineas,
                                    through="CotizacionCotizacionLineas", through_fields=("cotizacion", "linea"), editable=False)

    def __str__(self):
        return self.cliente.apellido + ", " + self.cliente.nombre + " | " + self.fecha.strftime("%d/%m/%y %H:%M:%S")

    def cantidad_personas(self):
        return int(self.cantidad_adultos) + int(self.cantidad_menores) + int(self.cantidad_menores_de_4)

    @property
    def total(self):
        costo = Decimal(0.00)
        """
        for categoria in CategoriaGastronomia.objects.filter(level=0):
            if CotizacionLineas.objects.filter(cotizacion__id=self.id,categoria__id=categoria.id).count() > 0:
                costo += categoria.costo * self.cantidad_personas()

            for linea in CotizacionLineas.objects.filter(cotizacion__id=self.id,categoria__id=categoria.id):
                if linea.gastronomia.costo:
                    if linea.cantidad:
                        costo += linea.gastronomia.costo * self.cantidad_personas() * linea.cantidad
                    else:
                        costo += linea.gastronomia.costo * self.cantidad_personas()
        """

        costo += self.total_adultos
        costo += self.total_menores

        logger.info("")
        logger.info("Sumando Salon %s con costo %s" % (self.salon, self.salon.costo))
        logger.info("%s + %s" % (costo, self.salon.costo))
        costo += self.salon.costo

        logger.info("")
        logger.info("Sumando Escenario %s con costo %s" % (self.escenario, self.escenario.costo))
        logger.info("%s + %s" % (costo, self.escenario.costo))
        costo += self.escenario.costo

        # Suma items que se calculan en el total, osea que no se multiplican por cantidad de personas sino
        # por cantidad de items
        for linea in CotizacionLineas.objects.filter(cotizacion__id=self.id, gastronomia__suma_calculo_total=True):
            logger.info("Sumando %s con costo %s" % (linea.gastronomia, linea.gastronomia.costo))
            logger.info("%s + (%s * %s)" % (costo, linea.gastronomia.costo, linea.cantidad))
            costo += linea.gastronomia.costo * linea.cantidad

        return costo

    @property
    def total_adultos(self):
        costo = Decimal(0.00)
        # Traemos las categorias de esta cotizacion
        categorias = list(set([x.categoria for x in Cotizacion.objects.get(pk=self.id).lineas.all() if x.categoria.adultos]))
        logger.info("")
        logger.info("Armando cotizacion N° %s" % self.id)
        logger.info("Adultos: %s" % (self.cantidad_adultos))

        for categoria in categorias:

            # Si la categoria no tiene costo asociado, se presupone que las gastronomias lo tienen
            if categoria.costo == 0:
                lineas = CotizacionLineas.objects.filter(cotizacion__id=self.id, categoria__id=categoria.id,
                                                         gastronomia__adultos=True)
                for linea in lineas:
                    # Si la gastronomia existe y tiene costo mayor a cero
                    if linea.gastronomia and linea.gastronomia.costo > 0:
                        logger.info("Agregando %s - %s | Costo %s a Cotizacion N° %s" % (
                        linea.categoria, linea.gastronomia, linea.gastronomia.costo, self.id))

                        if linea.gastronomia.con_cantidad:
                            logger.info("%s + (%s * %s adultos * %s items)" % (costo, linea.gastronomia.costo, self.cantidad_adultos, linea.cantidad))
                            costo += linea.gastronomia.costo * self.cantidad_adultos * linea.cantidad
                        else:
                            logger.info("%s + (%s * %s adultos)" % (costo, linea.gastronomia.costo, self.cantidad_adultos))
                            costo += linea.gastronomia.costo * self.cantidad_adultos

            # Si la categoria tiene costo asociado, traemos las lineas de esa categoria
            else:
                if categoria and categoria.costo > 0:

                    """
                    lineas = CotizacionLineas.objects.filter(cotizacion__id=self.id, categoria__id=categoria.id,
                                                             categoria__adultos=True)
                    for linea in lineas:
                        logger.info("Agregando %s | Costo %s a Cotizacion N° %s" % (
                            linea.categoria, linea.categoria.costo, self.id))

                        logger.info("%s + (%s * %s)" % (costo, linea.categoria.costo, self.cantidad_adultos))
                        costo += linea.categoria.costo * self.cantidad_adultos
                    """
                    logger.info("Agregando %s | Costo %s a Cotizacion N° %s" % (
                        categoria, categoria.costo, self.id))

                    logger.info("%s + (%s * %s)" % (costo, categoria.costo, self.cantidad_adultos))

                    costo += categoria.costo * self.cantidad_adultos

                    lineas = CotizacionLineas.objects.filter(cotizacion__id=self.id, categoria__id=categoria.id, gastronomia__adultos=True,
                                                    gastronomia__costo__gt=0)
                    for linea in lineas:
                        logger.info("%s + (%s * %s adultos)" % (costo, linea.gastronomia.costo, self.cantidad_adultos))
                        costo += linea.gastronomia.costo * self.cantidad_adultos
        logger.info("TOTAL ADULTOS: %s" % costo)
        return costo

    @property
    def total_menores(self):
        """
                categorias = CategoriaGastronomia.objects.filter(menores=True, level=0)
                costo = Decimal(0.00)

                for categoria in categorias:

                    # Suma costo de categoria
                    if CotizacionLineas.objects.filter(cotizacion__id=self.id, categoria__id=categoria.id).count() > 0:
                        if categoria.costo_2:
                            costo += categoria.costo * self.cantidad_menores

                    # Suma costos de gastronomias individuales
                    for linea in CotizacionLineas.objects.filter(cotizacion__id=self.id, categoria__id=categoria.id):
                        if linea.gastronomia.costo:
                            if linea.cantidad:
                                costo += linea.gastronomia.costo * self.cantidad_menores * linea.cantidad
                            else:
                                costo += linea.gastronomia.costo * self.cantidad_menores
                return costo
        """

        costo = Decimal(0.00)
        categorias = list(set([x.categoria for x in Cotizacion.objects.get(pk=self.id).lineas.all() if x.categoria.menores]))

        logger.info("")
        logger.info("Armando cotizacion N° %s" % self.id)
        logger.info("Menores: %s" % (self.cantidad_menores))

        for categoria in categorias:

            # Si la categoria no tiene costo asociado, se presupone que las gastronomias lo tienen
            if categoria.costo == 0:
                lineas = CotizacionLineas.objects.filter(cotizacion__id=self.id, categoria__id=categoria.id, gastronomia__menores=True)

                for linea in lineas:
                    # Si la gastronomia existe y tiene costo mayor a cero
                    if linea.gastronomia and linea.gastronomia.costo > 0:
                        logger.info("Agregando %s - %s | Costo %s a Cotizacion N° %s" % (linea.categoria, linea.gastronomia, linea.gastronomia.costo, self.id))

                        if linea.gastronomia.con_cantidad:
                            logger.info("%s + (%s * %s menores * %s items)" % (
                            costo, linea.gastronomia.costo, self.cantidad_menores, linea.cantidad))
                            costo += linea.gastronomia.costo * self.cantidad_menores * linea.cantidad
                        else:
                            logger.info("%s + (%s * %s)" % (costo, linea.gastronomia.costo, self.cantidad_menores))
                            costo += linea.gastronomia.costo * self.cantidad_menores

            # Si la categoria tiene costo asociado
            else:
                # Si solo tiene costo para adultos
                if categoria.costo_2 == 0:
                    if categoria and categoria.costo > 0:
                        logger.info("Agregando %s | Costo %s a Cotizacion N° %s" % (
                        categoria, categoria.costo, self.id))

                        logger.info("%s + (%s * %s)" % (costo, categoria.costo, self.cantidad_menores))
                        costo += categoria.costo * self.cantidad_menores
                else:
                    if categoria and categoria.costo_2 > 0:
                        """
                        lineas = CotizacionLineas.objects.filter(cotizacion__id=self.id, categoria__id=categoria.id,
                                                                 categoria__menores=True)
                        for linea in lineas:
                            logger.info("Agregando %s | Costo para menores: %s a Cotizacion N° %s" % (
                                linea.categoria, linea.categoria.costo_2, self.id))

                            logger.info("%s + (%s * %s)" % (costo, linea.categoria.costo_2, self.cantidad_menores))
                            costo += linea.categoria.costo_2 * self.cantidad_menores
                        """
                        costo += categoria.costo_2 * self.cantidad_menores

        logger.info("TOTAL MENORES: %s" % costo)
        return costo


    def categorias(self):
        lista = list(set(self.lineas.all().values_list('categoria')))
        return [CategoriaGastronomia.objects.get(pk=x[0]) for x in lista]

    class Meta:
        verbose_name_plural = "Cotizaciones"


class CotizacionCotizacionLineas(models.Model):
    cotizacion = models.ForeignKey(Cotizacion, on_delete=models.CASCADE)
    linea = models.ForeignKey(CotizacionLineas, on_delete=models.CASCADE)
