import locale

from graphene.types.resolver import dict_resolver
from graphene_django import DjangoObjectType
import graphene
from .models import Cliente, Gastronomia, TipoDeEvento, Formato, Salon, CategoriaGastronomia, Escenario, \
    Cotizacion, CotizacionLineas, CotizacionCotizacionLineas
from datetime import datetime
from django.db.models import Q

locale.setlocale(locale.LC_ALL, "es_AR.UTF-8")

f = '{:n}'.format


class ClienteType(DjangoObjectType):
    class Meta:
        model = Cliente

    nombre_completo = graphene.String()

    def resolve_nombre_completo(self, info):
        return str(self)

class CotizacionType(DjangoObjectType):
    class Meta:
        model = Cotizacion

    cantidad_de_personas = graphene.Int()
    cantidad_adultos = graphene.Int()
    cantidad_menores = graphene.Int()
    cantidad_menores_de_4 = graphene.Int()
    total = graphene.String()
    total_adultos = graphene.String()
    total_menores = graphene.String()
    total_infantes = graphene.String()
    fecha_formateada = graphene.String()

    def resolve_cantidad_de_personas(self, info):
        return self.cantidad_adultos + self.cantidad_menores + self.cantidad_menores_de_4

    def resolve_fecha_formateada(self, info):
        return self.fecha.strftime("%d/%m/%Y")

    def resolve_total(self, info):
        return f(self.total)

    def resolve_total_adultos(self, info):
        return f(self.total_adultos)

    def resolve_total_menores(self, info):
        return f(self.total_menores)

class CotizacionLineaType(DjangoObjectType):
    class Meta:
        model = CotizacionLineas

class CotizacionLineaObjectType(graphene.ObjectType):
    gastronomias = graphene.List(graphene.String)
    categoria = graphene.String()

class EscenarioType(DjangoObjectType):
    class Meta:
        model = Escenario

    costo = graphene.String()

    def resolve_costo(self, info):
        return f(self.costo)

class ItemType(graphene.ObjectType):
    class Meta:
        default_resolver = dict_resolver

    id = graphene.String()
    nombre = graphene.String()
    descripcion = graphene.String()

class CategoriaType(graphene.ObjectType):
    class Meta:
        default_resolver = dict_resolver

    id = graphene.String()
    nombre = graphene.String()
    items = graphene.List(ItemType)
    descripcion = graphene.String()

class CotizacionPorCategoriaType(graphene.ObjectType):
    class Meta:
        default_resolver = dict_resolver

    id = graphene.String()
    categoria = graphene.String()
    subcategorias = graphene.List(CategoriaType)
    cliente = graphene.Field(ClienteType)


class TipoDeEventoType(DjangoObjectType):
    class Meta:
        model = TipoDeEvento

    def resolve_nombre(self, info):
        if self.descripcion:
            return "%s (%s)" % (self.nombre, self.descripcion)
        return self.nombre

class FormatoType(DjangoObjectType):
    class Meta:
        model = Formato

class SalonType(DjangoObjectType):
    class Meta:
        model = Salon

    costo = graphene.String()

    def resolve_costo(self, info):
        return f(self.costo)

class GastronomiaType(DjangoObjectType):
    class Meta:
        model = Gastronomia

    categoria_id = graphene.Int()

    def resolve_categoria_id(self, info):
        return self.categoria.id

class SubCategoriaGastronomiaType(DjangoObjectType):
    class Meta:
        model = CategoriaGastronomia



class CategoriaGastronomiaType(DjangoObjectType):
    class Meta:
        model = CategoriaGastronomia

    gastronomias = graphene.List(GastronomiaType)
    categorias = graphene.List(SubCategoriaGastronomiaType)

    def resolve_gastronomias(self, info):
        return self.gastronomias.filter(level=0)

    def resolve_categorias(self, info):
        return self.categorias.filter(mostrar=True)


def procesa_categorias(cotizacion_id, islas):
    categorias = CategoriaGastronomia.objects.filter(level=0)
    x = []
    for categoria in categorias:

        y = {}
        y["id"] = categoria.id
        y["categoria"] = categoria.nombre
        y["subcategorias"] = []

        # Si es categoria son items incluidos
        if categoria.nombre[-1:] == '.':

            lineas = CotizacionLineas.objects.filter(cotizacion__id=cotizacion_id, categoria__id=categoria.get_children()[0].id)
            for linea in lineas:
                for item in linea.categoria.gastronomias.all():
                    z = {
                        "nombre": item.nombre,
                        "descripcion": item.descripcion,
                        "id": item.id
                    }
                    y["subcategorias"].append(z)

            print(categoria.get_children()[0].id)
            x.append(y)
        else:
            islas = CategoriaGastronomia.objects.filter(es_isla=True,  nombre__contains="Isla", parent_id=categoria.id)
            if len(islas) > 0:
                lineas = CotizacionLineas.objects.filter(cotizacion__id=cotizacion_id, categoria__nombre__contains="Isla")
                if len(lineas) > 0:
                    z = {
                        "nombre": 'Islas',
                    }

                    z["items"] = [{"nombre": x.categoria.nombre, "id": x.categoria.id} for x in lineas]

                    y["subcategorias"].append(z)
            if categoria.get_children():
                subcategorias = categoria.get_children()
                for subcategoria in subcategorias:
                    lineas = CotizacionLineas.objects.filter(cotizacion__id=cotizacion_id, gastronomia__categoria__id=subcategoria.id)
                    if len(lineas) > 0:
                        z = {
                            "nombre": subcategoria.nombre,
                            "id": subcategoria.id
                        }

                        z["items"] = [{"nombre": x.gastronomia.nombre, "id": x.gastronomia.id, "descripcion": x.gastronomia.descripcion} for x in lineas]

                        y["subcategorias"].append(z)

            else:
                lineas = CotizacionLineas.objects.filter(cotizacion__id=cotizacion_id,
                                                         gastronomia__categoria__id=categoria.id)
                for linea in lineas:
                    z = {}
                    if linea.cantidad != None and linea.cantidad > 0:
                        z["nombre"] = "%s - Cantidad: %s" % (linea.gastronomia.nombre, linea.cantidad)
                    else:
                        z["nombre"] = linea.gastronomia.nombre
                    z["id"] = linea.gastronomia.id
                    z["descripcion"] = linea.gastronomia.descripcion
                    y["subcategorias"].append(z)

            x.append(y)
    return [_ for _ in x if len(_["subcategorias"]) > 0]
    #return x


class Query(graphene.ObjectType):
    tipo_de_eventos = graphene.List(TipoDeEventoType)
    formatos = graphene.List(FormatoType)
    salones = graphene.List(SalonType)
    categoria_gastronomia = graphene.Field(CategoriaGastronomiaType, nombre=graphene.String())
    escenarios = graphene.List(EscenarioType)
    cotizacion = graphene.Field(CotizacionType, id=graphene.Int())
    cotizacion_por_categoria = graphene.List(CotizacionPorCategoriaType, cotizacion_id=graphene.Int())

    def resolve_tipo_de_eventos(self, info):
        return TipoDeEvento.objects.all()

    def resolve_formatos(self, info):
        return Formato.objects.all()

    def resolve_salones(self, info):
        return Salon.objects.all()

    def resolve_categoria_gastronomia(self, info, **kwargs):
        nombre = kwargs.get('nombre')
        return CategoriaGastronomia.objects.get(nombre=nombre)

    def resolve_escenarios(self, info):
        return Escenario.objects.all()

    def resolve_cotizacion(self, info, id):
        return Cotizacion.objects.get(pk=id)



    def resolve_cotizacion_por_categoria(self, info, cotizacion_id):
        # result = []
        categorias = CategoriaGastronomia.objects.filter(Q(level=0) | Q(es_isla=True))

        islas = CotizacionPorCategoriaType()
        #islas.lineas = []
        #islas.categoria = CategoriaGastronomia.objects.filter(es_isla=True).first().parent.nombre

        #for categoria in categorias:
        #    result.append(procesa_categoria(categoria, cotizacion_id, islas))
        #result.append(procesa_categorias(cotizacion_id, islas))
        result = procesa_categorias(cotizacion_id, islas)

        #if len(islas.lineas) > 0:
        #    for item in result:
        #        if item.categoria == "Buffet por Islas":
        #            item.lineas = item.lineas + islas.lineas

            #result.append(islas)

        return [x for x in result if x != None]

class CrearCliente(graphene.Mutation):
    class Arguments:
        nombre = graphene.String()
        apellido = graphene.String()
        email = graphene.String()
        telefono = graphene.String()



    ok = graphene.Boolean()
    cliente = graphene.Field(lambda: ClienteType)

    def mutate(self, info, nombre, apellido, email, telefono):
        cliente = Cliente.objects.get_or_create(nombre=nombre, apellido=apellido,email=email,telefono=telefono)
        ok = True
        return CrearCliente(cliente=cliente, ok=ok)


class CotizacionMutation(graphene.Mutation):
    class Arguments:
        nombre = graphene.String()
        apellido = graphene.String()
        email = graphene.String()
        telefono = graphene.String()
        fecha = graphene.String()
        formato_id = graphene.Int()
        salon_id = graphene.Int()
        tipo_de_evento_id = graphene.Int()
        escenario_id = graphene.Int()
        cantidad_adultos = graphene.Int()
        cantidad_menores = graphene.Int()
        cantidad_menores_de_4 = graphene.Int()
        observaciones = graphene.String()

    ok = graphene.Boolean()
    cotizacion = graphene.Field(CotizacionType)

    def mutate(self, info, nombre, apellido, email, telefono, fecha, formato_id, salon_id, tipo_de_evento_id, escenario_id,
               cantidad_adultos, cantidad_menores, cantidad_menores_de_4, observaciones):
        cliente, created = Cliente.objects.get_or_create(nombre=nombre, apellido=apellido, email=email, telefono=telefono)

        cotizacion = Cotizacion.objects.create(
            cliente=cliente,
            fecha=fecha,
            formato_id=formato_id,
            salon_id=salon_id,
            tipo_de_evento_id=tipo_de_evento_id,
            escenario_id=escenario_id,
            cantidad_adultos=cantidad_adultos,
            cantidad_menores=cantidad_menores,
            cantidad_menores_de_4=cantidad_menores_de_4,
            observaciones=observaciones
        )
        ok = True
        return CotizacionMutation(cotizacion=cotizacion, ok=ok)


class LineaCotizacionMutation(graphene.Mutation):
    class Arguments:
        cotizacion_id = graphene.Int()
        gastronomia_id = graphene.Int()
        categoria_id = graphene.Int()
        valor = graphene.Boolean()
        cantidad = graphene.String()

    ok = graphene.Boolean()
    cotizacion_id = graphene.Int()
    cotizacion_lineas_id = graphene.Int()
    linea = graphene.Field(CotizacionLineaType)

    def mutate(self, info, cotizacion_id, gastronomia_id, categoria_id, valor, cantidad):
        cotizacion = Cotizacion.objects.get(pk=cotizacion_id)
        gastronomia = Gastronomia.objects.get(pk=gastronomia_id)
        categoria = CategoriaGastronomia.objects.get(pk=categoria_id)

        linea, linea_created = CotizacionLineas.objects.get_or_create(
            gastronomia=gastronomia,
            categoria=categoria
        )

        linea.valor = valor
        linea.cantidad = cantidad
        linea.save()

        cotizacion_linea, created_cotizacion_linea = CotizacionCotizacionLineas.objects.get_or_create(
            linea=linea,
            cotizacion=cotizacion
        )

        return LineaCotizacionMutation(ok=True, cotizacion_lineas_id=cotizacion_linea.id, cotizacion_id=cotizacion.id, linea=linea)


class LineasCotizacionType(graphene.InputObjectType):
    cotizacion_id = graphene.Int(required=True)
    gastronomia_id = graphene.Int(required=False)
    categoria_id = graphene.Int(required=True)
    parent_categoria_id = graphene.Int(required=False)
    valor = graphene.Boolean(required=False)
    cantidad = graphene.String(required=False)

def limpiar_lineas_de_categoria(categoria_id=0, cotizacion_id=0):
    CotizacionCotizacionLineas.objects.filter(
        Q(cotizacion_id=cotizacion_id) & Q(Q(linea__categoria_id=categoria_id) | Q(linea__categoria__parent_id=categoria_id))).delete()


class LineasCotizacionMutation(graphene.Mutation):
    class Arguments:
        categoria_id = graphene.String()
        cotizacion_id = graphene.Int()
        lineas = graphene.List(LineasCotizacionType)

    ok = graphene.Boolean()
    message = graphene.String()

    @staticmethod
    def mutate(self, info, categoria_id, cotizacion_id, lineas):
        try:
            #Si pertenece a varias categorias
            if '|' in categoria_id:
                for id in categoria_id.split('|'):
                    limpiar_lineas_de_categoria(int(id), cotizacion_id)
            else:
                limpiar_lineas_de_categoria(int(categoria_id), cotizacion_id)

            for linea in lineas:
                cotizacion = Cotizacion.objects.get(pk=linea.cotizacion_id)
                categoria = CategoriaGastronomia.objects.get(pk=linea.categoria_id)
        
                if linea.gastronomia_id == 0:
                #if categoria.es_isla:

                    cotizacion_linea, linea_created = CotizacionLineas.objects.get_or_create(
                        gastronomia=None,
                        categoria=categoria
                    )
                else:
                    gastronomia = Gastronomia.objects.get(pk=linea.gastronomia_id)
                    cotizacion_linea, linea_created = CotizacionLineas.objects.get_or_create(
                        gastronomia=gastronomia,
                        categoria=categoria
                    )

                cotizacion_linea.valor = linea.valor
                cotizacion_linea.cantidad = linea.cantidad
                guardado = cotizacion_linea.save()
    
                cotizacion_cotizacion_linea, created_cotizacion_cotizacion_linea = CotizacionCotizacionLineas.objects.get_or_create(
                    linea=cotizacion_linea,
                    cotizacion=cotizacion
                )
                pass
        except Exception as e:
            return LineasCotizacionMutation(ok=False, message=e)
        return LineasCotizacionMutation(ok=True, message="Linea/s guardadas correctamente")

class HayasMutations(graphene.ObjectType):
    crear_cliente = CrearCliente.Field()
    crear_cotizacion = CotizacionMutation.Field()
    cargar_linea = LineaCotizacionMutation.Field()
    cargar_lineas = LineasCotizacionMutation.Field()

schema = graphene.Schema(query=Query, mutation=HayasMutations)