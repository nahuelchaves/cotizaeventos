from io import BytesIO
import locale
from reportlab.lib.units import cm, mm
from reportlab.pdfgen import canvas
from reportlab.platypus import SimpleDocTemplate, Paragraph, ListFlowable, ListItem, Spacer, Image
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.enums import TA_CENTER
from django.http import HttpResponse
from cotizaEventos.models import Cotizacion, CategoriaGastronomia, CotizacionLineas
from reportlab.lib.pagesizes import letter, A4, LEGAL
from reportlab.lib.utils import ImageReader

from django.shortcuts import get_object_or_404
from django.contrib.staticfiles.finders import find

from cotizaEventos.schema import procesa_categorias

locale.setlocale(locale.LC_ALL, "es_AR.UTF-8")

f = '{:n}'.format

class LineaVM:
    gastronomia = str
    categoria = str
    costo = 0.00
    cantidad = 0


class CategoriaVM:
    nombre = str
    costo = 0.00
    costo_2 = 0.00
    lineas = []
    imagen = str


def obtener_categorias_y_gastronomias(cotizacion_id=None):
    result = []
    cotizacion = Cotizacion.objects.get(pk=cotizacion_id)
    for categoria in CategoriaGastronomia.objects.filter(level=0):
        coti_por_categoria = CategoriaVM()
        coti_por_categoria.nombre = categoria.nombre
        coti_por_categoria.costo = categoria.costo
        coti_por_categoria.costo_2 = categoria.costo_2
        coti_por_categoria.imagen = categoria.imagen
        lineas = []

        for linea in CotizacionLineas.objects.filter(cotizacion__id=cotizacion_id,categoria__id=categoria.id):
            linea_type = LineaVM()
            linea_type.gastronomia = linea.gastronomia.nombre
            linea_type.costo = linea.gastronomia.costo
            linea_type.categoria = linea.categoria.nombre
            linea_type.cantidad = linea.cantidad
            lineas.append(linea_type)

        for linea in CotizacionLineas.objects.filter(cotizacion__id=cotizacion_id,categoria__parent__id=categoria.id):
            linea_type = LineaVM()
            #linea_type.gastronomia = linea.gastronomia.nombre
            #linea_type.costo = linea.gastronomia.costo
            linea_type.categoria = linea.categoria.nombre
            linea_type.cantidad = linea.cantidad
            lineas.append(linea_type)

        coti_por_categoria.lineas = lineas
        result.append(coti_por_categoria)
    
    return result


def imprime_cotizacion(request, id):
    # Create the HttpResponse object with the appropriate PDF headers.
    response = HttpResponse(content_type='application/pdf')
    # response['Content-Disposition'] = 'attachment; filename="somefilename.pdf"'

    # Get Data
    cotizacion = get_object_or_404(Cotizacion, pk=id)
    categorias_cotizacion = obtener_categorias_y_gastronomias(cotizacion_id=cotizacion.id)
    result = procesa_categorias(id, None)

    buffer = BytesIO()

    doc = SimpleDocTemplate(buffer,
        rightMargin=72,
        leftMargin=72,
        topMargin=72,
        bottomMargin=72,
        pagesize=LEGAL)
    doc.width, doc.height = LEGAL

    # Our container for 'Flowable' objects
    elements = []

    # A large collection of style sheets pre-made for us
    styles = getSampleStyleSheet()
    styles.add(ParagraphStyle(name='centered', alignment=TA_CENTER))

    # Draw things on the PDF. Here's where the PDF generation happens.
    # See the ReportLab documentation for the full list of functionality.
    # users = User.objects.all()
    elements.append(Paragraph('Cotizacion N°%s' % cotizacion.id, styles['Heading1']))
    elements.append(Paragraph('DATOS CLIENTE', styles['Heading3']))

    elements.append(Paragraph('Nombre y Apellido: %s' % cotizacion.cliente, styles['Normal']))
    elements.append(Paragraph('E-mail: %s' % cotizacion.cliente.email, styles['Normal']))
    elements.append(Paragraph('Teléfono: %s' % cotizacion.cliente.telefono, styles['Normal']))

    elements.append(Paragraph('DATOS DEL EVENTO', styles['Heading3']))
    elements.append(Paragraph('Fecha Evento: %s' % cotizacion.fecha.strftime('%d/%m/%Y'), styles['Normal']))
    elements.append(Paragraph('Cantidad de Adultos: <strong>%s</strong>' % cotizacion.cantidad_adultos, styles['Normal']))
    elements.append(Paragraph('Cantidad de Menores (de 4 a 16): <strong>%s</strong>' % cotizacion.cantidad_menores, styles['Normal']))
    elements.append(Paragraph('Cantidad de Menores de 4: <strong>%s</strong>' % cotizacion.cantidad_menores_de_4, styles['Normal']))
    elements.append(Paragraph('Cantidad de Personas: <strong>%s</strong>' % cotizacion.cantidad_personas(), styles['Normal']))
    elements.append(Paragraph('Tipo de Evento: %s' % cotizacion.tipo_de_evento, styles['Normal']))
    elements.append(Paragraph('Formato: %s' % cotizacion.formato, styles['Normal']))
    elements.append(Paragraph('Salón: %s' % cotizacion.salon, styles['Normal']))
    elements.append(Paragraph('Escenario: %s' % cotizacion.escenario, styles['Normal']))
    elements.append(Paragraph('Observaciones: %s' % cotizacion.observaciones, styles['Normal']))

    elements.append(Paragraph('GASTRONOMÍA', styles['Heading3']))

    categorias_cotizacion = obtener_categorias_y_gastronomias(cotizacion_id=cotizacion.id)

    for categoria in result:
        elements.append(Spacer(width=0, height=10))
        elements.append(Paragraph(categoria["categoria"], styles["Normal"]))

        for subcategoria in categoria["subcategorias"]:
            nombre_subcat = " - %s" % subcategoria["nombre"]
            if ("descripcion" in subcategoria):
                nombre_subcat = nombre_subcat + " %s" % subcategoria["descripcion"]
            elements.append(Paragraph(nombre_subcat, styles["Normal"]))

            if "items" in subcategoria:
                items = ", ".join([x["nombre"] for x in subcategoria["items"]])
                elements.append(Paragraph(items, styles["Normal"]))
    """
    
    for categoria in categorias_cotizacion:
        if len(categoria.lineas) > 0:
            elements.append(Spacer(width=0, height=10))

            if categoria.nombre != 'Coffee Break':
                #total += categoria.costo * cotizacion.cantidad_personas()
                pass
            if categoria.costo:
                if categoria.costo_2 > 0:
                    elements.append(Paragraph('Categoria: %s: <strong>Adultos: $%s - Menores: $%s</strong>' % (categoria.nombre, f(categoria.costo), f(categoria.costo_2)), styles['Normal']))
                else:
                    elements.append(Paragraph('Categoria: %s: <strong>$%s por persona</strong>' % (categoria.nombre, f(categoria.costo)), styles['Normal']))
            else:
                elements.append(Paragraph('Categoria: %s' % categoria.nombre, styles['Normal']))

            elements.append(Spacer(width=0, height=10))
            items = []

            for linea in categoria.lineas:
                if linea.costo:
                    #total += linea.costo * cotizacion.cantidad_personas()
                    if type(linea.cantidad) is int and linea.cantidad > 0:
                        items.append(ListItem(Paragraph('%s: $<strong>%s</strong> x %s item/s' % (linea.gastronomia, linea.costo, linea.cantidad), styles['Normal']), value='circle'))
                    else:
                        items.append(ListItem(Paragraph('%s: $<strong>%s</strong>' % (linea.gastronomia, linea.costo), styles['Normal']), value='circle'))
                else:
                    items.append(ListItem(Paragraph('%s' % linea.gastronomia, styles['Normal']), value='circle'))
                       
            for linea_categoria_completa in CotizacionLineas.objects.filter(cotizacion__id=cotizacion.id, categoria__parent__nombre=categoria.nombre):
                items.append(ListItem(Paragraph(linea_categoria_completa.categoria.nombre, styles['Normal'])))
            
            my_list = ListFlowable(items, bulletType='bullet', start='circle')
            elements.append(my_list)
    """

    elements.append(Paragraph('COTIZACIÓN FINAL', styles['Heading2']))
    elements.append(Paragraph('TOTAL ADULTOS: <strong>$%s</strong>' % f(cotizacion.total_adultos), styles['Normal']))
    elements.append(Paragraph('TOTAL MENORES: <strong>$%s</strong>' % f(cotizacion.total_menores), styles['Normal']))
    # elements.append(Paragraph('TOTAL INFANTES: <strong>$%s</strong>' % cotizacion.total_infantes, styles['Normal']))
    elements.append(Paragraph('SALON: <strong>$%s</strong>' % f(cotizacion.salon.costo), styles['Normal']))
    elements.append(Paragraph('ESCENARIO: <strong>$%s</strong>' % f(cotizacion.escenario.costo), styles['Normal']))
    elements.append(Paragraph('TOTAL: <strong>$%s</strong>' % f(cotizacion.total), styles['Normal']))

    #elements.append(Paragraph('Forma de pago: %s' % 'xx.xxx', styles['Normal']))
    #elements.append(Paragraph('Validez de presupuesto de pago: %s' % '7 días', styles['Normal']))

    elements.append(Spacer(width=0, height=10))

    elements.append(Paragraph('- Validez presupuesto por 30 días', styles['Normal']))
    elements.append(Paragraph('- Forma de pago: a convenir', styles['Normal']))
    elements.append(Paragraph('- Todos los precios son finales, en pesos y con impuestos incluidos.', styles['Normal']))
    elements.append(Paragraph('- No se incluyen aranceles de SADAIC ni ADICAPIF. De corresponder, corren por cuenta de los organizadores.', styles['Normal']))
    elements.append(Paragraph('- Ponemos a nuestra disposición, a todo nuestro equipo para colaborar en lo que necesiten, a fin de que el evento sea un éxito y que la experiencia de los asistentes en el Fin del Mundo sea inolvidable.', styles['Normal']))
    """
    COTIZACIÓN FINAL
    TOTAL: $xx.xxx
    Forma de pago: xxxxx
    Validez de presupuesto: xxxxx
    """

    doc.build(elements)

    # Get the value of the BytesIO buffer and write it to the response.
    pdf = buffer.getvalue()
    buffer.close()
    response.write(pdf)
    return response



def drawHeader(c, id):
    logo = ImageReader(find('LASHAYAS_cotizador_pdf_logo.jpg'))

    c.setFillColor("#134A50")

    c.setFont('Helvetica-Bold', 8)
    c.drawString(480, 800 + 180, "Contacto")
    c.setFont('Helvetica', 8)

    c.drawString(480, 790 + 180, "Luis F.Martial 1650,")
    c.drawString(480, 778 + 180, "9410, Ushuaia, TDF")
    c.drawString(480, 767 + 180, "02901 442000")
    c.drawString(480, 756 + 180, "eventos@lashayas.com.ar")

    c.drawImage(logo, 40, 720 + 180, mask='auto', width=110, height=100)

    c.setFont('Helvetica', 20)
    c.drawString(50, 700 + 180, "Cotización de Evento N° %s" % id)

    c.setFont('Helvetica-Bold', 8)
    c.drawString(480, 700 + 180, "Cotización realizada por:")
    c.setFont('Helvetica', 8)
    c.drawString(480, 688 + 180, "Administrador")
    c.drawString(480, 676 + 180, "Hoja: %s de 3" % c.getPageNumber())


def imprime_cotizacion_5(request, id):
    locale.setlocale(locale.LC_ALL, "es_AR.UTF-8")

    cotizacion = get_object_or_404(Cotizacion, pk=id)
    categorias_cotizacion = obtener_categorias_y_gastronomias(cotizacion_id=cotizacion.id)

    result = procesa_categorias(id, None)
    # import json; print(json.dumps(result))
    response = HttpResponse(content_type='application/pdf')
    buffer = BytesIO()
    c = canvas.Canvas(buffer, pagesize=LEGAL)

    drawHeader(c, id)





    c.setFont('Helvetica-Bold', 8)
    c.setFillColor("#134A50")

    c.drawString(50, 630 + 180, "CLIENTE")

    c.setStrokeColor("#E3EBF1")
    c.line(50, 625  + 180, 560, 625 + 180)
    c.setFont('Helvetica', 8)
    c.setFillColor("#586780")

    c.drawString(50, 610  + 180, "Nombre y Apellido: %s" % cotizacion.cliente)
    c.drawString(50, 598  + 180, "E - mail: %s" % cotizacion.cliente.email)
    c.drawString(50, 586  + 180, "Teléfono: %s" % cotizacion.cliente.telefono)
    c.drawString(50, 574  + 180, "Observaciones: %s" % cotizacion.observaciones)


    c.setFont('Helvetica-Bold', 8)
    c.setFillColor("#134A50")

    c.drawString(50, 540 + 180, "EVENTO")
    c.line(50, 535 + 180, 560, 535 + 180)

    # FECHA EVENTO

    fecha_evento = ImageReader(find('fecha_evento.jpg'))

    c.drawImage(fecha_evento, 43, 480 + 180, mask='auto', width=45, height=45)
    c.setFont('Helvetica-Bold', 8)
    c.setFillColor("#586780")

    c.drawString(50, 470 + 180, "Fecha Evento")

    c.setFont('Helvetica', 8)
    c.drawString(50, 458 + 180, cotizacion.fecha.strftime('%d %B de %Y'))

    # CANTIDAD PERSONAS

    cantidad_personas = ImageReader(find('cantidad_personas.jpg'))

    c.drawImage(cantidad_personas, 130, 480 + 180, mask='auto', width=45, height=45)
    c.setFont('Helvetica-Bold', 8)

    c.drawString(133, 470 + 180, "Cant. personas")
    c.setFont('Helvetica-Oblique', 8)

    c.drawString(133, 460 + 180, "Adultos:")
    c.drawString(133, 450 + 180, "Menores (4 a 12 años):")
    c.drawString(133, 430 + 180, "Infantes ( - 4 años):")

    c.setFont('Helvetica', 8)
    c.drawString(133, 440 + 180, str(cotizacion.cantidad_menores))
    c.drawString(170, 460 + 180, str(cotizacion.cantidad_adultos))
    c.drawString(133, 420 + 180, str(cotizacion.cantidad_menores_de_4))





    # TIPO DE EVENTO

    tipo_de_evento = ImageReader(find('tipo_de_evento.jpg'))

    c.drawImage(tipo_de_evento, 210, 480 + 180, mask='auto', width=45, height=45)
    c.setFont('Helvetica-Bold', 8)
    c.drawString(215, 470 + 180, "Tipo de Evento")
    c.setFont('Helvetica', 8)

    c.drawString(215, 460 + 180, str(cotizacion.tipo_de_evento.nombre))

    # FORMATO

    formato = ImageReader(find('formato.jpg'))

    c.drawImage(formato, 300, 480 + 180, mask='auto', width=45, height=45)
    c.setFont('Helvetica-Bold', 8)
    c.drawString(305, 470 + 180, "Formato")
    c.setFont('Helvetica', 8)
    c.drawString(305, 460 + 180, str(cotizacion.formato))


    # LUGAR

    lugar = ImageReader(find('lugar.jpg'))

    c.drawImage(lugar, 380, 480 + 180, mask='auto', width=45, height=45)
    c.setFont('Helvetica-Bold', 8)
    c.drawString(393, 470 + 180, "Lugar")
    c.setFont('Helvetica', 8)
    c.drawString(393, 460 + 180, str(cotizacion.salon))

    # ESCENARIO / TARIMA

    escenario_tarima = ImageReader(find('escenario_tarima.jpg'))

    c.drawImage(escenario_tarima, 470, 480 + 180, mask='auto', width=45, height=45)
    c.setFont('Helvetica-Bold', 8)
    c.drawString(475, 470 + 180, "Escenario / Tarima")
    c.setFont('Helvetica', 8)
    c.drawString(475, 460 + 180, str(cotizacion.escenario))

    c.setFont('Helvetica-Bold', 8)
    c.setFillColor("#134A50")

    c.drawString(50, 380 + 180, "GASTRONOMÍA")

    c.setFont('Helvetica-Oblique', 8)
    c.setFillColor("#586780")
    c.drawString(120, 380 + 180, "(Ver detalle en siguiente hoja)")

    c.line(50, 375 + 180, 560, 375 + 180)

    c.setFillColor("#134A50")

    categorias = [x for x in categorias_cotizacion if len(x.lineas) > 0]

    imagen_y = 320 + 180
    imagen_x = 48
    titulo_y = 310 + 180
    titulo_x = 50

    y_post_categorias = 270

    if len(list(filter(lambda x: "Menú" in x.nombre, categorias_cotizacion))):
        print("TIENE MENUS ADULTOS")

    categoria_imagen = ImageReader(find("menu_adultos.jpg"))
    c.drawImage(categoria_imagen, imagen_x, imagen_y, mask='auto', width=45, height=45)
    c.setFont('Helvetica-Bold', 8)
    c.setFillColor("#586780")

    c.drawString(titulo_x, titulo_y, "Menú Adultos")

    titulo_x += 80
    imagen_x += 80

    for categoria in [x for x in categorias if "Menú" not in x.nombre]:
        # MENU ADULTOS
        if (imagen_x > 600):
            imagen_x = 48
            titulo_x = 50
            imagen_y = 260 + 180
            titulo_y = 250 + 180
            y_post_categorias -= 60
        if categoria.imagen:
            categoria_imagen = ImageReader(find(categoria.imagen))

            c.drawImage(categoria_imagen, imagen_x, imagen_y, mask='auto', width=45, height=45)

        c.setFont('Helvetica-Bold', 8)
        c.setFillColor("#586780")

        c.drawString(titulo_x, titulo_y, categoria.nombre)

        titulo_x += 80
        imagen_x += 80
        print(imagen_x, categoria.nombre)

    c.setFont('Helvetica-Bold', 8)
    c.setFillColor("#134A50")

    c.drawString(50, y_post_categorias + 180, "ORGANIZACIÓN ESTIMATIVA DEL SALÓN")

    c.setFont('Helvetica-Oblique', 8)
    c.setFillColor("#586780")
    c.drawString(220, y_post_categorias + 180, "(Ver última hoja)")

    y_post_categorias -= 5

    c.line(50, y_post_categorias + 180, 560, y_post_categorias + 180)





    c.setFont('Helvetica-Bold', 8)
    c.setFillColor("#134A50")

    y_post_categorias -= 45

    c.drawString(50, y_post_categorias + 180, "COTIZACIÓN FINAL")

    y_post_categorias -= 5

    c.line(50, y_post_categorias + 180, 560, y_post_categorias + 180)

    y_post_categorias -= 33

    c.drawString(60, y_post_categorias + 180, "TOTAL: $%s.-" % cotizacion.total)

    c.setFillColor("#586780")
    c.setStrokeColor("#586780")
    c.setFillAlpha(0.3)
    c.setStrokeAlpha(0.3)

    y_post_categorias -= 8

    c.rect(50, y_post_categorias + 180, 560, 22, stroke=1, fill=1)

    c.setFillColor("#586780")

    y_post_categorias -= 24
    c.drawString(50, y_post_categorias + 180, "Formas de pago: a convenir")

    y_post_categorias -= 10
    c.drawString(50, y_post_categorias + 180, "Validez presupuesto por 30 días")

    c.showPage()

    drawHeader(c, id)

    c.setFont('Helvetica-Bold', 8)

    c.setFillColor("#134A50")

    # LISTADO DE GASTRONOMIA

    c.drawString(50, 630 + 180, "GASTRONOMÍA")

    gastronomia_y = 600 + 180
    for categoria in result:
        c.setFont('Helvetica-Bold', 8)

        if gastronomia_y < 230:
            c.showPage()
            drawHeader(c, id)
            gastronomia_y = 600 + 180
            c.setFont('Helvetica-Bold', 8)

        c.drawString(50, gastronomia_y, categoria["categoria"])
        gastronomia_y -=20

        for subcategoria in categoria["subcategorias"]:
            c.setFont('Helvetica-Bold', 8)
            nombre_subcat = " - %s" % subcategoria["nombre"]

            if ("descripcion" in subcategoria):
                nombre_subcat = nombre_subcat + " %s" % subcategoria["descripcion"]

            c.drawString(50, gastronomia_y, nombre_subcat)
            subcat_length = c.stringWidth(nombre_subcat)

            if "items" in subcategoria:
                c.setFont('Helvetica', 7)
                items = ", ".join([x["nombre"] for x in subcategoria["items"]])
                c.drawString(subcat_length + 70, gastronomia_y, items)

            gastronomia_y -= 20
        gastronomia_y -= 10

    c.showPage()
    drawHeader(c, id)

    c.setFont('Helvetica-Bold', 8)
    c.setFillColor("#134A50")

    c.drawString(50, 630 + 180, "ORGANIZACIÓN ESTIMATIVA DEL SALÓN")

    if (cotizacion.salon.diagrama):
        imagen_salon = ImageReader(cotizacion.salon.diagrama.file)
        iw, ih = imagen_salon.getSize()
        aspect = ih / float(iw)
        c.drawImage(imagen_salon, 50, 400 + 180, width=16*cm, height=(16*cm * aspect))
    c.save()

    pdf = buffer.getvalue()
    buffer.close()
    response.write(pdf)
    return response