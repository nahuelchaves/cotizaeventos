from django.contrib import admin
from django.forms import ModelMultipleChoiceField

from cotizaEventos.models import Cliente, Gastronomia, CotizacionCotizacionLineas, CotizacionLineas, \
    Cotizacion, CategoriaGastronomia, Salon,TipoDeEvento, Formato, Escenario
from mptt.admin import MPTTModelAdmin, DraggableMPTTAdmin, TreeRelatedFieldListFilter
from mptt.fields import TreeNodeChoiceField
from django import forms
from django.contrib.admin import AdminSite
from django.utils.translation import ugettext_lazy
from django.contrib.auth.models import User, Group
from django.contrib.auth.admin import UserAdmin, GroupAdmin
from django.conf import settings

class MyAdminSite(AdminSite):
    # Text to put at the end of each page's <title>.
    site_title = ugettext_lazy('Las Hayas - Cotizador de Eventos')

    # Text to put in each page's <h1> (and above login form).
    site_header = ugettext_lazy('Las Hayas - Cotizador de Eventos')

    # Text to put at the top of the admin index page.
    index_title = ugettext_lazy('Modulos de Cotización')


admin_site = MyAdminSite()
admin_site.site_url = 'http://18.228.91.49' if settings.DEBUG == False else 'http://127.0.0.1:3000'
admin_site.register(User, UserAdmin)
admin_site.register(Group, GroupAdmin)

class ClienteAdmin(admin.ModelAdmin):
    pass


class SubcategoriaInline(admin.TabularInline):
    model = CategoriaGastronomia
    extra = 1
    fields = ['nombre', 'orden', 'es_isla']


class CategoriaGastronomiaAdmin(DraggableMPTTAdmin):
    """def get_queryset(self, request):
        qs = super(CategoriaGastronomiaAdmin, self).get_queryset(request)
        return qs.filter(es_principal=True)
    """
    fields = ['nombre', 'es_principal', 'es_isla', 'costo', 'costo_2', 'imagen', 'mostrar', 'adultos', 'menores']
    list_filter = ['es_principal']
    inlines = [SubcategoriaInline]

    def get_inline_instances(self, request, obj=None):
        if request.user.is_superuser:
            return super(CategoriaGastronomiaAdmin, self).get_inline_instances(request, obj)
        return []

    def get_readonly_fields(self, request, obj=None):
        if request.user.is_superuser:
            return super(CategoriaGastronomiaAdmin, self).get_readonly_fields(request, obj)
        else:
            return ('nombre','created_at', 'created_by')

    def get_fields(self, request, obj=None):
        fields = list(super(CategoriaGastronomiaAdmin, self).get_fields(request, obj))
        exclude_set = set()
        if obj:  # obj will be None on the add page, and something on change pages
            user = request.user
            if obj.gastronomias_tienen_costo and user.is_superuser == False:
                exclude_set.add('costo')
                exclude_set.add('costo_2')
            if user.is_superuser == False:
                exclude_set.add('menores')
                exclude_set.add('adultos')
                exclude_set.add('es_principal')
                exclude_set.add('es_isla')
                exclude_set.add('imagen')
                exclude_set.add('mostrar')
                exclude_set.add('infantes')
                exclude_set.add('gastronomias')


        return [f for f in fields if f not in exclude_set]


class CategoriasEnArbol(forms.ModelForm):
  categoria = TreeNodeChoiceField(queryset=CategoriaGastronomia.objects.all())


class GastronomiaAdmin(DraggableMPTTAdmin):
    form = CategoriasEnArbol
    list_filter = (
        ('categoria', TreeRelatedFieldListFilter),
    )
    fields = [
        'nombre',
        'descripcion',
        'con_cantidad',
        'categoria',
        'incluida',
        'costo',
        'suma_calculo_total',
        'adultos',
        'menores'
    ]
    search_fields = ['nombre']
    list_per_page = 50

    def get_readonly_fields(self, request, obj=None):
        if request.user.is_superuser:
            return super(GastronomiaAdmin, self).get_readonly_fields(request, obj)
        else:
            return ('nombre', 'descripcion', 'categoria', 'created_at', 'created_by')

    def get_fields(self, request, obj=None):
        fields = list(super(GastronomiaAdmin, self).get_fields(request, obj))
        exclude_set = set()
        if obj:  # obj will be None on the add page, and something on change pages
            user = request.user
            if user.is_superuser == False:
                exclude_set.add('con_cantidad')
                exclude_set.add('incluida')
                exclude_set.add('suma_calculo_total')

        return [f for f in fields if f not in exclude_set]

"""
class CotizacionCotizacionLineasAdmin(admin.ModelAdmin):
    categoria = TreeNodeChoiceField(queryset=CategoriaGastronomia.objects.all())

    pass
"""

class CotizacionLineaAdmin(admin.TabularInline):
    model = Cotizacion.lineas.through
    extra = 0
    readonly_fields = ('linea',)
    # autocomplete_fields = ["linea"]




class CotizacionAdmin(admin.ModelAdmin):
    inlines = (CotizacionLineaAdmin,)
    list_filter = ("cliente", )
    search_fields = ("cliente", )
    list_display = ("id", "fecha", "__str__", "cliente")


class TipoDeEventoAdmin(admin.ModelAdmin):
    pass


class FormatoAdmin(admin.ModelAdmin):
    pass


class SalonAdmin(admin.ModelAdmin):
    pass


admin_site.register(TipoDeEvento, TipoDeEventoAdmin)
admin_site.register(Formato, FormatoAdmin)
admin_site.register(Salon, SalonAdmin)
admin_site.register(Cliente, ClienteAdmin)
admin_site.register(Escenario)
admin_site.register(CategoriaGastronomia, CategoriaGastronomiaAdmin)
admin_site.register(Gastronomia, GastronomiaAdmin)
admin_site.register(CotizacionLineas)
admin_site.register(Cotizacion, CotizacionAdmin)
# admin_site.register(CotizacionCotizacionLineas, CotizacionCotizacionLineasAdmin)