from django.test import TestCase
from cotizaEventos.models import Cotizacion, Cliente, Formato, Salon, TipoDeEvento, Escenario, \
                                 CotizacionLineas, Gastronomia, CategoriaGastronomia, CotizacionCotizacionLineas
from datetime import datetime

class CotizacionTestCase(TestCase):
    fixtures = ["categorias", "gastronomias", "tipos"]

    def setUp(self):
        Cliente.objects.create(apellido="TestApellido", nombre="Nombre", email="test@test.com", telefono="123123")

    def crear_cotizacion(self):
        cliente = Cliente.objects.first()
        formato = Formato.objects.first()
        salon = Salon.objects.first()
        tipo_de_evento = TipoDeEvento.objects.first()
        escenario = Escenario.objects.first()
        cotizacion = Cotizacion.objects.create(
            cliente=cliente,
            fecha=datetime.now(),
            formato=formato,
            salon=salon,
            tipo_de_evento=tipo_de_evento,
            escenario=escenario
        )
        return cotizacion

    def test_create_cotizacion(self):
        cotizacion = self.crear_cotizacion()

        self.assertIsInstance(cotizacion, Cotizacion)


    def test_add_linea_to_cotizacion(self):
        cotizacion = self.crear_cotizacion()

        self.assertIsNotNone(cotizacion)
        gastronomia = Gastronomia.objects.first()
        categoria = CategoriaGastronomia.objects.first()


        linea = CotizacionLineas.objects.create(
            gastronomia=gastronomia,
            valor="1",
            categoria=categoria
        )

        cotizacion_linea = CotizacionCotizacionLineas.objects.create(
            linea=linea,
            cotizacion=cotizacion
        )

        self.assertEqual(cotizacion_linea.cotizacion_id, 1)
        self.assertEqual(cotizacion_linea.linea_id, 1)


    def test_calcular_cotizacion(self):
        cotizacion = Cotizacion.objects.get(pk=5)
        cotizacion.total_adultos