import { ApolloLink } from 'apollo-link'
import { HttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
/*
function getCookie(name) {
  var cookieValue = null;
  if (document.cookie && document.cookie != '') {
    var cookies = document.cookie.split(';');
    for (var i = 0; i < cookies.length; i++) {
      var cookie = cookies[i].trim(); //$.trim(cookies[i]);
      // Does this cookie string begin with the name we want?
      if (cookie.substring(0, name.length + 1) == (name + '=')) {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}*/

export default (ctx) => {
  const httpLink = new HttpLink({
    uri: '/graphql',
    credentials: 'same-origin'
  });



  // middleware
  const middlewareLink = new ApolloLink((operation, forward) => {
    const token = '' // process.server ? ctx.req.session : window.__NUXT__.state.session;

    operation.setContext({
      headers: {
        authorization: `Bearer ${token}`,
      }
    });
    return forward(operation)
  });
  const link = middlewareLink.concat(httpLink);
  return {
    link,
    cache: new InMemoryCache()
  }
}
