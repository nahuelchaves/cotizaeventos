export const state = () => ({
  datos: {},
  id: null,
  lineas: {}
});

export const mutations = {
  setCotizacion (state, form) {
    state.datos = form
  },
  setCotizacionId (state, id) {
    state.id = id;
  },
  setCotizacionLinea(state, payload) {
    console.log('setCotizacionLinea', payload);
    state.lineas[payload.linea] = payload.datos;
  },
  clearCotizacion(state) {
    state.datos = {};
    state.id = null;
    state.lineas = {};
  }
};
