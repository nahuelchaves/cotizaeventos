import Vue from 'vue'
import Vuetify from 'vuetify'
import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify, {
  theme: {
    primary: colors.grey.lighten1,
    secondary: colors.grey.darken1,
    accent: colors.shades.black,
    error: colors.red.accent3,
    info: colors.teal.lighten1,
    warning: colors.amber.base,
    success: colors.green.accent3
  }
})
