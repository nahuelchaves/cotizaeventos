from cotizaEventos.admin import admin_site
from django.contrib import admin

from config.models import Cotizacion
# Register your models here.

class CotizacionAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        return False if self.model.objects.count() > 0 else super().has_add_permission(request)

#admin_site.register(Cotizacion, CotizacionAdmin)
