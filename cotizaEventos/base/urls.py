from django.conf import settings
from django.urls import path
from django.conf.urls import include
from django.conf.urls.static import static
from cotizaEventos.urls import urlpatterns as cotizaeventos
from cotizaEventos.admin import admin_site
from graphene_django.views import GraphQLView
from . import views

urlpatterns = [
      path('admin/', admin_site.urls),
      path('graphql', GraphQLView.as_view(graphiql=True)),
      path('', views.FrontendAppView.as_view()),
    ] + cotizaeventos + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        path(r'^__debug__/', include(debug_toolbar.urls)),
    ]


if 'rosetta' in settings.INSTALLED_APPS:
    urlpatterns += [
        path('rosetta/', include('rosetta.urls')),
    ]
