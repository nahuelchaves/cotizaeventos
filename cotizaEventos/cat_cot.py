from cotizaEventos.models import CategoriaGastronomia, Cliente, Cotizacion, CotizacionCotizacionLineas, CotizacionLineas, Escenario, Formato, Gastronomia, Salon, TipoDeEvento
from django.db.models import Q
import json

categorias = CategoriaGastronomia.objects.filter(Q(level=0) | Q(es_isla=True))
x = []
cotizacion_id=31
for categoria in categorias:
    lineas = CotizacionLineas.objects.filter(cotizacion__id=cotizacion_id, categoria__id=categoria.id)
    if len(lineas) > 0:
        y = {}
        y["id"] = categoria.id
        y["categoria"] = categoria.nombre
        y["subcategorias"] = []
        if categoria.get_children():
            subcategorias = categoria.get_children()
            for subcategoria in subcategorias:
                z = {
                    "nombre": subcategoria.nombre,
                    "id": subcategoria.id
                }
                # z["items"] = CotizacionLineas.objects.filter(cotizacion__id=cotizacion_id, categoria__id=subcategoria.id).values()
                z["items"] = [{"nombre":x.gastronomia.nombre} for x in lineas]

                y["subcategorias"].append(z)

        x.append(y)

print(json.dumps(x))
